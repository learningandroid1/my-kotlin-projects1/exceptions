class Employee(val name: String, val salary: Int = 100000) {
    fun callToClient(clientName: String){
        println("Сотрудник $name: звоню клиенту $clientName")
    }
}

fun findEmployeeBySalary(employees: List<Employee>, salary: Int): Employee{
    for (employee in employees)
        if (employee.salary == salary)
        return employee

    throw Exception("Нет сотрудника с зарплатой $salary")
}

fun main(){
 val employee1 = Employee("Kirill", 50000)
 val employee2 = Employee("Olga", 70000)
 val employee3 = Employee("Oleg", 100000)

 val employees = listOf<Employee>(employee1, employee2, employee3)

 try {findEmployeeBySalary(employees, 170000)}
 catch (e: Exception){
     println(e.message)
 }
}