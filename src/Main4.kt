import kotlin.random.Random

fun main() {
//    val list = List<Int>(5) { Random.nextInt() }
//    list[10]

//    val item = runCatching { list[10] }
//    println(item)
//    println(item.isSuccess)
    val numbers = Numbers(10)
    printNumber(numbers.getNumberByIndex(20))
}

fun printNumber(number: Int) = println("number - $number")

class Numbers (n: Int) {
    private val numbers = List(n) { Random.nextInt() }
    fun getNumberByIndex(index: Int) = numbers[index]
}