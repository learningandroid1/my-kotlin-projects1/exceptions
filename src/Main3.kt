import java.lang.IndexOutOfBoundsException
import kotlin.random.Random

fun main() {
    val list = List<Int>(5) { Random.nextInt() }
    try {
        for (i in list.indices)
            println(list[i])
        println("End of program")
    } catch (e: IndexOutOfBoundsException) {
        println("There is no such index")
    } catch (e: NullPointerException) {
        println("Null pointer")
    } catch (t: Throwable) {
        println("basic throwable")
    }
    finally {
        println("Finally block execution")
    }



}